from .base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': root('mixins/db.sqlite3'),
    }
}

STATICFILES_DIRS = [root('mixins/static')]
