$(function() {

	var csrftoken = $.cookie('csrftoken'); 
	
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if (!isNotMethodForUpdateResources(settings.type) && !this.crossDomain){
				xhr.setRequestHeader('X-CSRFToken', csrftoken);
			}
		}
	});

	function isNotMethodForUpdateResources(method) {
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
});