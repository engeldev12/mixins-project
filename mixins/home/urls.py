from django.urls import path

from . import views
from .api import views as views_api

app_name = 'home'

urlpatterns = [
	path('', views.HomeIndexView.as_view(), name='index'),
	path('about', views.AboutView.as_view(), name='about'),
	path('contact', views.ContactView.as_view(), name='contact'),
	path('artciles', views.ArticleListView.as_view(), name='articles-list'),
	path('article/<int:pk>/download', views.ArticleDownloadView.as_view(), name='article-download'),
	path('article/<int:pk>/watch-online', views.ArticleWatchOnlineView.as_view(), name='article-watch-online'),
	path('add-contact', views_api.ContactCreateAPIView.as_view(), name='add-contact'),
]
