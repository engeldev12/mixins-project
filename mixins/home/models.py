from django.db import models


class Contact(models.Model):
	email = models.EmailField(unique=True)

	def __str__(self):
		return self.email
		

class Article(models.Model):
	name = models.CharField(max_length=80, unique=True)
	file = models.BooleanField(default=False)

	def __str__(self):
		return self.name