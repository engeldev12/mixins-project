const headers = {
    "X-CSRFToken": $.cookie("csrftoken"),
    "Content-Type": "application/json"
};


new Vue({
	el: '#app',
	delimiters: ['${', '}'],
	data: {
		contact: {},
		error: "",
		ocurrioUnError: false
	},
	methods: {

		newContact() {
			this.showContactForm();
		},

		showContactForm() {
			this.cleanContactForm();
			$('#modal-contact').modal('open');
		},

		cleanContactForm() {
			this.contact = {};
			this.error = "";
		},

		send() {
			var vm = this;
			axios.post("/add-contact",
			    { email: this.contact.email },
				{ headers: headers }
			)
			.then(function(response) { 
				vm.showMessageSuccess()
			})
			.catch(function(e) {
				vm.showErrorMessage(e);
			});
		},

		showMessageSuccess() {
			this.cleanContactForm();
			alert('Tu contacto ha sido enviado exitosamente!');
			$('#modal-contact').modal('close');
		},

		showErrorMessage(e) {
			this.error = this.getError(e);
			this.ocurrioUnError = true;
		},

		getError(e) {
			return e.response.data.email[0];
		}
	}
});
