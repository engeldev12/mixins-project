from django import forms
from .models import Contact


class ContactForm(forms.ModelForm):
	
	class Meta:
		model = Contact
		fields = ['email']
		widgets = {
			'email': forms.EmailInput(attrs={
						'class': 'form-control',
						'placeholder': 'example@example.com'
					}
				),
		}