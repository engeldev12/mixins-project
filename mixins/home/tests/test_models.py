from django.test import TestCase
from ..models import Contact
from django.db import IntegrityError


class ContactTest(TestCase):

	def test_da_error_si_no_tiene_un_email(self):

		contact = Contact(email='')

		self.assertFalse(contact.save())

	def test_da_error_al_intentar_agregar_un_email_existente(self):

		Contact.objects.create(email='engel@email.com')

		with self.assertRaises(IntegrityError):
			Contact.objects.create(email='engel@email.com')
