from django.test import TestCase
from ..forms import ContactForm
from ..models import Contact


class ContactFormTest(TestCase):
	
	def test_sin_email_da_error(self):

		data = {
			'email': ''
		}

		form = ContactForm(data)

		self.assertFalse(form.is_valid())
		self.assertIn('Este campo es requerido.', form.errors['email'])

	def test_un_email_repetido_da_error(self):
		
		Contact.objects.create(email='engel@emial.com')

		data = {
			'email': 'engel@emial.com'
		}

		form = ContactForm(data)

		form.is_valid()

		self.assertIn('Contact con esta Email ya existe.', form.errors['email'])
