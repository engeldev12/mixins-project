from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views import generic

from .models import Article
from .forms import ContactForm


class HomeMixin(object):

	def get_context_data(self, **kwargs):
		context = super(HomeMixin, self).get_context_data(**kwargs)
		context['number_of_visit'] = 100
		return context

	def get(self, request, *args, **kwargs):
		return super().get(request, *args, **kwargs)


class HomeIndexView(HomeMixin, generic.TemplateView):
	template_name = 'home/index.html'


class AboutView(HomeMixin, generic.TemplateView):
	template_name = 'home/about.html'
	

class ArticleListView(generic.ListView):
	model = Article
	template_name = 'home/articles-list.html' 
	context_object_name = 'articles'

	def get_queryset(self):
		return Article.objects.all()


class ArticleMixin(object):
	model = Article
	context_object_name = 'article'

	def build_response(self, article):
		return render(self.request, self.template_name, {'article': article})

	def get(self, request, pk, *args, **kwargs):
		article = get_object_or_404(Article, pk=pk)

		if not article.file:
			return render(request,
					'home/article-unavailable.html',
					{'article': article}
				)
		
		response = self.build_response(article)

		return response


class ArticleDownloadView(ArticleMixin, generic.DetailView):
	template_name = 'home/article-detail.html'


class ArticleWatchOnlineView(ArticleMixin, generic.DetailView):
	template_name = 'home/article-watch-online.html'

	def build_response(self, article):
		context = {
			'article': article,
			'desde': 'ArticleWatchOnlineView'
		}
		return render(self.request, self.template_name, context)


class ContactView(HomeMixin, generic.TemplateView):
	template_name = 'home/contact.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['form'] = ContactForm()
		return context
