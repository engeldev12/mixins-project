from rest_framework import generics

from .serializers import ContactSerializer


class ContactCreateAPIView(generics.CreateAPIView):
	serializer_class = ContactSerializer
