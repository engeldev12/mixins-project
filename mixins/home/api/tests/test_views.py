from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework.test import APIClient

from ...models import Contact


class ContactCreateView(APITestCase):

	def setUp(self):
		self.url = reverse('home:add-contact')
		self.client = APIClient()

	def test_da_error_cuando_no_tiene_email_el_contacto(self):

		data = {
			'email': ''
		}

		response = self.client.post(self.url, data)

		self.assertEqual(400, response.status_code)

	def test_da_error_cuando_el_email_ya_existe(self):

		Contact.objects.create(email='engel@email.com')
		data = {
			'email': 'engel@email.com'
		}

		response = self.client.post(self.url, data)

		self.assertEqual(400, response.status_code)

	def test_cuando_no_es_un_emial_valido_da_error(self):
		data = {
			'email': 'email-invalido'
		}

		response = self.client.post(self.url, data)

		self.assertEqual(400, response.status_code)

	def test_cuando_el_email_es_valido_se_puede_agregar_el_contacto(self):

		data = {
			'email': 'engel@email.com'
		}

		response = self.client.post(self.url, data)

		self.assertEqual(201, response.status_code)
